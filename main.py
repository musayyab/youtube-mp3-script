from bs4 import BeautifulSoup
import requests
import pandas as pd
import youtube_dl
import re

# define global parameters
URL = 'https://ahmadiyya.de'
URL = 'https://ahmadiyya.de/mediathek/videos/browse'
PAGE_NUMBER = 1


def increment_page():
    global PAGE_NUMBER
    PAGE_NUMBER = PAGE_NUMBER+1


def is_there_next(soup):
    search_next = soup.find('span', {'class': 'pbarrowright'})
    if search_next:
        return True
    return None


def return_next_page(soup):
    """
    return next_url if pagination continues else return None

    Parameters
    -------
    soup - BeautifulSoup object - required

    Return 
    -------
    next_url - str or None if no next page
    """
    global PAGE_NUMBER
    next_url = None
    if is_there_next(soup):
        next_url = URL + '/' + str(PAGE_NUMBER)
        increment_page()

    return next_url


def download_mp3(link):
    print("Link detected")
    print("The video " + link + " is downloading...")
    opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }]
    }
    youtube_dl.YoutubeDL(opts).download([link])


def get_all_videos(url):
    soup = BeautifulSoup(requests.get(url).content, 'html.parser')
    search_videos = soup.findAll('img', src=re.compile('img.youtube.com'))
    for video in search_videos:
        video_id = video['src'].split('/')[4]
        video_link = 'https://youtu.be/' + video_id
        download_mp3(video_link)


def create_soup(url):
    """
    iterate over each review, extract out content, and handle next page logic 
    through recursion

    Parameters
    -------
    url - str - required
        input url
    """
    soup = BeautifulSoup(requests.get(url).content, 'html.parser')
    get_all_videos(url)
    next_url = return_next_page(soup)
    if next_url is not None:
        print('Getting videos from page: ' + next_url)
        create_soup(next_url)


create_soup(URL)